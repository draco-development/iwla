<?php	

// this function returns random numbers for the image rotator on the main page
function randomNumber($min, $max, $sizeOfArray) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $sizeOfArray);
}

function countPictures(){
	$directory = 'images/rotator/';
	return ($files=glob($directory.'*.jpg')) ? count($files) - 1 : 0;
}
function verifyUser($db, $usernameTableName){ //this checks to make sure that the user has not been deleted manually by an admin. If it has, it kicks the user out.
	$verifyUser = "SELECT * FROM `" . $usernameTableName . "` WHERE `username` = '".$_SESSION['username']."'";
	$runVerifyUser = mysqli_query($db, $verifyUser);
	if(mysqli_num_rows($runVerifyUser) == 1){ // checks to see if username is still registered
		$debugMessage = 'still in database';
	} else {
		$_SESSION['loggedIn'] = 0; // this lowers the flag to log a user out if they've been manually removed from the database.
		$debugMessage = 'Bitch been kicked';
	}
//	echo $debugMessage;
}

//This function checks the username and the password against the database
function loginVerification ($db, $tablename, $username, $password, $loginPage, $successRedirect){
    
    // Clean POST data
	$username = mysqli_real_escape_string($db, trim($username));
    
    // query to check for username in database
    $query = "SELECT * FROM `$tablename` WHERE `username` = '$username'";
    $result = mysqli_query($db, $query);
    $userInfo = mysqli_fetch_array($result);
	
    // check to see if user is in database
    if (mysqli_num_rows($result) < 1){
	    	    
        $_SESSION['loginError'] = "That Username and Password does not match!"; // set error log for display on login page
        header("Location: " . $loginPage . "?error=notInDatabase"); // redirect user back to login if not in database
        exit;
        
    } else {  //if username is in the database, check for matching password
	    
	    $_SESSION['username'] = $userInfo['username'];
	    
        if (password_verify($password, $userInfo['password'])){ 	        
	        
			$_SESSION['loggedIn'] = 1; // raise flag for sucessful login
			$_SESSION['loginError'] = 0; //delete login error flag if it had been raised
			header("Location: " . $successRedirect . "?loggedIn=".$_SESSION['loggedIn']);
		} else {						
            $_SESSION['loggedIn'] = 0; // lower flag for failed login
			$_SESSION['loginError'] = "That Username and Password does not match!"; // set error log for display on login page
	        header("Location: " . $loginPage);
        }
    }
}


//from C&C store functions:
function ipLockoutCounter($db) { // this sets the lockout time to now + 5 minutes
	$date = date('Y-m-d'); // Catching date for query use
	$time = date('H:i:s'); // catching time for query use
	$customer = $_POST['user']; //catching attempted username
	$unlockTime = date('H:i:s',strtotime('+5 minutes',time())); // add 5 minutes to logged time for the unlock timer.
	$logTheIpQuery = "INSERT INTO ipLog (`ipAddress`, `entryDate`, `date`, `time`, `unlockTime`, `userIdAttempt`) VALUES ('".$_SERVER["REMOTE_ADDR"]."', '$date', '$date', '$time', '$unlockTime', '$customer')";
	$runLogTheIpQuery = mysqli_query($db, $logTheIpQuery);
	}
function clearLockoutTimer($db){
	$clearTheLogQuery = "UPDATE `C250209_ccdist`.`ipLog` SET `date`='0000-00-00' WHERE `ipAddress` = '".$_SERVER["REMOTE_ADDR"]."'";
	$runClearTheLogQuery = mysqli_query($db, $clearTheLogQuery);
	}
function loginAttempts($db){
	$date = date('Y-m-d'); // Catching date for query use
	$rowCountQuery = "SELECT * FROM ipLog WHERE ipAddress = '".$_SERVER["REMOTE_ADDR"]."' AND date = '".$date."'";
	$runRowCountQuery = mysqli_query($db, $rowCountQuery);
	$loginAttempts = mysqli_num_rows($runRowCountQuery);
	return $loginAttempts;
	}
	function sessionTimeout(){
# Check for session timeout, else initialize time
	if (isset($_SESSION['timeout'])) {	
		# Check Session Time for expiry
		# Time is in seconds. 10 * 60 = 600s = 10 minutes
		if ($_SESSION['timeout'] + 60 * 60 < time()){
			$_SESSION['loggedIn'] = 0;
			session_destroy();
		}
	}
	else {
		# Initialize time
		$_SESSION['timeout']=time();
		//test
	}
}
