const heros = [
  "/images/property/hiRes/hikingBridge.jpg",
  "/images/property/hiRes/archeryTargets.jpg",
  "/images/property/hiRes/chapterHouse.jpg",
  "/images/property/hiRes/enterance.jpg",
  "/images/property/hiRes/gunRangeWithRifle.jpg",
  "/images/property/hiRes/hikingBench.jpg",
  "/images/property/hiRes/pond.jpg",
  "/images/property/hiRes/hikingTeepee.jpg",
  "/images/property/hiRes/pond2.jpg",
  "/images/property/hiRes/pond3.jpg",
  "/images/property/hiRes/rifleAndPistolBench.jpg",
  "/images/property/hiRes/rifleandPistolsign.jpg",
  "/images/property/hiRes/trailSign.jpg",
  "/images/property/hiRes/trailSign2.jpg",
  "/images/property/hiRes/trailSign3.jpg",
  "/images/property/hiRes/trailSign4.jpg",
  "/images/property/hiRes/trapandskeet2.jpg",
  "/images/property/hiRes/trapandskeet3.jpg"
];
let lastHero = "/images/property/hiRes/archeryTargets.jpg"; // set this as the bg image set for .bg2 container

// this will run every 10 seconds and switches images based on which one is currently showing
if (screen.width > 1100) {
  setInterval(function() {
    // this toggles the opacity of the two images to switch the containers
    $(".bg1").toggleClass("show");
    $(".bg2").toggleClass("show");

    // this loads a new random hero into the container that is not showing
    let nextHero = getRandomHero();
    if ($(".bg1").hasClass("show")) {
      setTimeout(function() {
        $(".bg2").css({
          background: `center/cover url(${nextHero}) no-repeat`
        });
        lastHero = nextHero;
      }, 3000);
    } else {
      setTimeout(function() {
        $(".bg1").css({
          background: `center/cover url(${nextHero}) no-repeat`
        });
        lastHero = nextHero;
      }, 3000);
    }
  }, 13000);
}
const getRandomHero = () => {
  let newHero = lastHero;
  do {
    newHero = heros[Math.floor(Math.random() * 18)];
  } while (newHero == lastHero);

  return newHero;
};
