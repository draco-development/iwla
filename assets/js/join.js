// form types
const singleForm = ["intro", "applicant1", "applicant2", "submit"];
const familyForm = [
  "intro",
  "applicant1",
  "applicant2",
  "spouse1",
  "spouse2",
  "children",
  "submit"
];
// status variables
let currentStage = 0;
let memType = ""; // ST, SM, FM, SR
let formType = ""; // singleForm, familyForm
// form controls variables
const progressBar = $(".joinForm__progressBar");
const memRadios = $(".membership-selector__radio");
const next = $(".form-controls__next");
const back = $(".form-controls__back");
const submit = $(".joinForm__submitBtn");
// form sections variables
const introSect = $(".membership-selector");
const introSect_inputs = $(".membership-selector input");
const applicantSect1 = $(".single-form--1");
const applicantSect1_inputs = $(".single-form--1 input");
const applicantSect2 = $(".single-form--2");
const applicantSect2_inputs = $(".single-form--2 input");
const spouseSect1 = $(".spouse-form--1");
const spouseSect1_inputs = document.querySelectorAll(".spouse-form--1 input");
const spouseRequired1 = document.querySelectorAll(".spouse-form--1 [required]");
const spouseSect2 = $(".spouse-form--2");
const spouseSect2_inputs = document.querySelectorAll(".spouse-form--2 input");
const spouseRequired2 = document.querySelectorAll(".spouse-form--2 [required]");
const childrenSect = $(".children-form");
const childrenSect_inputs = document.querySelectorAll(".children-form input");
const childrenRequired = document.querySelectorAll(".children-form [required]");
const familyInputArr = [];

spouseRequired1.forEach(input => {
  familyInputArr.push(input);
});
spouseRequired2.forEach(input => {
  familyInputArr.push(input);
});
childrenRequired.forEach(input => {
  familyInputArr.push(input);
});

// error messages
const formError = $(".form__errorMessage");
const memTypeError = "Please choose a membership type.";

// trigger click on first radio on page load
buildProgressBar("singleForm");

//=====EVENT LISTENERS=======

//@ MEMBERSHIP RADIOS
$(memRadios).each(function() {
  $(this).click(function() {
    if (this.value == "FM") {
      formType = "familyForm";
      buildProgressBar(formType);
      familyInputArr.forEach(input => {
        input.required = true;
      });
    } else {
      formType = "singleForm";
      buildProgressBar(formType);
      familyInputArr.forEach(input => {
        input.required = false;
      });
    }
    memType = this.value;
    $(formError).hide(); // clear memType Error
  });
});

//@ NEXT
$(next).click(function() {
  const progressDots = $(".fa-circle");
  const statusBars = $("span.bar");

  $(formError).hide(); // clear validation error message
  // move on if section is valid
  if (validateSection(currentStage)) {
    // only process if button is active
    if (!$(this).hasClass("btn-disabled")) {
      currentStage++; // advance stage
      let formT =
        formType == "singleForm"
          ? singleForm[currentStage]
          : familyForm[currentStage];

      $(back).removeClass("btn-disabled"); // enable back button
      // check for submit stage
      if (formT == "submit") {
        $(this).addClass("btn-disabled"); // disable next button if at submit
        $(submit).show(); // show the submit button
      }
      renderForm(formT); // show correct section
      // section completed, color the progress bar
      $(progressDots).each(function(index) {
        if (index >= currentStage) {
          // only color dot of validated section
          return;
        }
        $(this).css("color", "#1ba03b"); // color the dot
      });
      // section completed, color the bar to the next section
      $(statusBars).each(function(index) {
        if (index >= currentStage - 1) {
          // only color bar if validated section
          return;
        }
        $(this).css("background-color", "#1ba03b"); // color the bar
      });
    }
  } else {
    $(formError)
      .html("Please complete all required fields.")
      .show();
  }
});

//@ BACK
$(back).click(function() {
  // only process if button is active
  if (!$(this).hasClass("btn-disabled")) {
    currentStage--; // advance stage
    let formT =
      formType == "singleForm"
        ? singleForm[currentStage]
        : familyForm[currentStage];

    $(submit).hide(); // hide the submit button
    console.log("currentStage = " + formT);
    $(next).removeClass("btn-disabled"); // enable back button
    // check for last stage
    if (formT == "intro") {
      $(this).addClass("btn-disabled"); // disable back button if at intro
    }
    renderForm(formT); // show correct section
  }
});

// FUNCTIONS
function validateSection(stage) {
  switch (stage) {
    case 0: // membership
      var isSectionValid;
      $(introSect_inputs).each(function() {
        if (this.checkValidity()) {
          isSectionValid = true; // we only need one mem type to be valid
        }
      });
      return isSectionValid;
    case 1: // applicant sect 1
      var isSectionValid = true;
      $(applicantSect1_inputs).each(function() {
        if (!this.checkValidity()) {
          isSectionValid = false;
        }
      });
      return isSectionValid;
    case 2: // applicant sect 2
      var isSectionValid = true;
      $(applicantSect2_inputs).each(function() {
        if (!this.checkValidity()) {
          isSectionValid = false;
        }
      });
      return isSectionValid;
    case 3: // spouse sect 1
      var isSectionValid = true;
      $(spouseSect1_inputs).each(function() {
        if (!this.checkValidity()) {
          isSectionValid = false;
        }
      });
      return isSectionValid;
    case 4: // spouse sect 2
      var isSectionValid = true;
      $(spouseSect2_inputs).each(function() {
        if (!this.checkValidity()) {
          isSectionValid = false;
        }
      });
      return isSectionValid;
    case 5: // children sect
      var isSectionValid = true;
      $(childrenSect_inputs).each(function() {
        if (!this.checkValidity()) {
          isSectionValid = false;
        }
      });
      return isSectionValid;
  }
}

function renderForm(stage) {
  if (stage != "submit") {
    $("fieldset").hide(); // hide all sections if not at submit
  }
  // render correct section based on stage
  switch (stage) {
    case "intro":
      $(introSect).show(); // show the form section
      $(introSect_inputs)[0].focus();
      break;
    case "applicant1":
      $(applicantSect1).show();
      $(applicantSect1_inputs)[0].focus();
      break;
    case "applicant2":
      $(applicantSect2).show();
      $(applicantSect2_inputs[0]).focus();
      break;
    case "spouse1":
      $(spouseSect1).show();
      $(spouseSect1_inputs[0]).focus();
      break;
    case "spouse2":
      $(spouseSect2).show();
      $(spouseSect2_inputs[0]).focus();
      break;
    case "children":
      $(childrenSect).show();
      $(childrenSect_inputs[0]).focus();
      break;
  }
}

function buildProgressBar(formTypeStr) {
  var dots = "";
  let formT = formTypeStr == "singleForm" ? singleForm : familyForm;

  $(progressBar).html(function() {
    // one dot per family form item
    $(formT).each(function(index) {
      if (index == formT.length - 1) {
        // dont render a dot for last section
        return;
      }
      if (index < formT.length - 2) {
        // only render bar in between sections
        dots += "<i class='fa fa-circle'></i><span class='bar'></span>";
      } else {
        dots += "<i class='fa fa-circle'></i>";
      }
    });
    return dots;
  });
}
