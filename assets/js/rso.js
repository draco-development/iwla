$('#new').click(function(){
    $('#newMemberForm').toggle("newMemberForm");
    $('#results').css("display", "none");
});

$('#submit').click(function(e){
    e.preventDefault();
    $('#newMemberForm').css("display", "none");
    $('#results').css("display", "inline");
});
