import "../scss/app.scss";

const $ = require("jquery");
global.$ = global.jQuery = $;

require("popper.js");
require("bootstrap");

// bundle all images with webpack
const imagesContext = require.context('../../public/images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);


alerts = JSON.parse(alerts); // this converts a twig array from template script

// if there are alerts, then load the first one
if (alerts) {
  showNextAlert();
}
/*=============================
======= EVENT LISTENERS =======
===============================*/

// Close alert click
$("#alertClose").click(function() {
  console.log("click close");
  // if there are more alerts, show the next one ELSE just fade out the alert
  if (alerts) {
    alerts.shift();
    $(".alert_container-outer").fadeOut(500);
    setTimeout(function() {
      showNextAlert();
    }, 500);
  } else {
    $(".alert_container-outer").fadeOut(500);
  }
});

// Mobile nav click for dropdown
$(".dropdown").click(function() {
  $(this)
    .children()
    .eq(1)
    .toggleClass("dropmenu-show");
});

/*=============================
======= HELPER FUNCTIONS =======
===============================*/

function showNextAlert() {
  if (alerts.length > 0) {
    $(".alert_list_item").remove();
    $(".alert_list").append(
      "<li class='alert_list_item'>" + alerts[0] + "</li>"
    );
    $(".alert_container-outer").fadeIn(1000);
  }
}
