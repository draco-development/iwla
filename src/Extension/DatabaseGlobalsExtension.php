<?php


namespace App\Extension;

use App\Entity\Globals;
use App\Entity\Links;
use App\Entity\SocialMedia;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class DatabaseGlobalsExtension extends AbstractExtension implements GlobalsInterface
{
    protected $entity;

    public function __construct(EntityManagerInterface $entity)
    {
        $this->entity = $entity;
    }

    public function getGlobals()
    {

        return [
            'globals' => $this->getGlobalVars(),
            'globalLinks' => $this->getLinks(),
            'socialMedia' => $this->getSocialMedia(),
        ];
    }

    public function getLinks()
    {
        $links = $this->entity->getRepository(Links::class)->findBy([], ['priority' => 'DESC', 'linkDisplay' => 'ASC'], '12');
        $returnArray = array();
        foreach($links as $link){
            $returnArray[$link->getLinkDisplay()] = $link->getLinkAddress();
        }

        return $returnArray;
    }

    public function getGlobalVars(){
        $globals = $this->entity->getRepository(Globals::class)->findAll();
        $returnArray = array();
        foreach($globals as $global){
            $returnArray[$global->getName()] = $global->getValue();
        }

        return $returnArray;
    }

    public function getSocialMedia(){

        return $this->entity->getRepository(SocialMedia::class)->findOneBy(['id' => 1]);

    }
}
