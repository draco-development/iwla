<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180924181132 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE UNIQUE INDEX uniqueActivity ON activities (activity_name)');
        $this->addSql('ALTER TABLE alerts CHANGE is_active is_active TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE outdoor_activities_events ADD details VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE outdoor_activities_hours CHANGE day day TINYINT(1) NOT NULL COMMENT \'0=sunday\', CHANGE closed closed TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX uniqueActivity ON activities');
        $this->addSql('ALTER TABLE alerts CHANGE is_active is_active TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE outdoor_activities_events DROP details');
        $this->addSql('ALTER TABLE outdoor_activities_hours CHANGE day day TINYINT(1) NOT NULL COMMENT \'0=sunday\', CHANGE closed closed TINYINT(1) DEFAULT \'0\' NOT NULL');
    }
}
