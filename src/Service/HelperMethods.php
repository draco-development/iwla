<?php


namespace App\Service;


use App\Entity\GoogleServiceAccountJson;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Google_Client;
use Google_Exception;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use mysqli;

class HelperMethods
{
    public function getPageImages($numberOfImages = 5, $pathToImages = 'images/rotator', array $searchStrings = null, bool $caseSensitive = false)
    {
        // Select all pictures in the rotator directory
        $rotatorImagePool = scandir($pathToImages);

        //Filter the contents of the folder to only return images
        foreach($rotatorImagePool as $image) {
            if(preg_match('/\.(jpg|jpeg|png)$/', $image)){
                $images[] = $image;
            }
        }

        // if the user wants to use a search string, iterate the results looking for each string
        if($searchStrings !== null){



            // loop through each searchstring
            foreach($searchStrings as $searchString){
                // and compare it to each image
                foreach($images as $k => $image){
                    //check the case sensitive flag for comparison script
                    if($caseSensitive){
                        if(strpos($image, $searchString) !== false){

                            // match was found, save it
                            $filteredImages[] = $image;

                            //Remove that element so we cannot have duplicates
                            unset($images[$k]);
                        }
                    } else {
                        if(strpos(strtolower($image), strtolower($searchString)) !== false){

                            // match was found, save it
                            $filteredImages[] = $image;

                            //Remove that element so we cannot have duplicates
                            unset($images[$k]);

                        } // end case insensitive check
                    } // end check for case sensitivity
                } // end image foreach
            } // end searchstring foreach
            $images = $filteredImages;
            unset($filteredImages);
        } // end searchstring code

        // Shuffle the results
        shuffle($images);

        // Take the lucky winners
        $images = array_slice($images,0,$numberOfImages);

        // Piece the path and image together
        foreach($images as $image){
            $output[] = $pathToImages."/".$image;
        }

        // Return a fully qualified link
        return $output;
    }

    public function getPageDetails($pageName, mysqli $db){

        $query = "SELECT header, details, exp_date, type FROM outdoor_activities_events WHERE activity = '$pageName';";
        $result = $db->query($query);

        $events = array();

        while($row = $result->fetch_array()){
            $events[] = $row;
        }
        return $events;
    }

    public function getHoursOfOperation($pageName, mysqli $db){

        $query = "SELECT day, opentime, closingtime, closed FROM outdoor_activities_hours WHERE activity = '$pageName';";
        $result = $db->query($query);

        $hours = array();

        while($row = $result->fetch_array()){
            $hours[] = $row;
        }
        return $hours;
    }

    // Return all Leaders and their positions
    public function getLeadership(mysqli $db){

        // Get a list of all leaders to populate the page
        $query = "SELECT members_leadership_rel.id, members.member_first_name, members.member_last_name, members.member_number, leadership_positions.position, leadership_positions.term_expiration, members_leadership_rel.elected_date FROM members_leadership_rel INNER JOIN members ON members_leadership_rel.member_id = members.member_number INNER JOIN leadership_positions ON members_leadership_rel.leadership_position = leadership_positions.position WHERE is_expired = 0 ORDER BY members_leadership_rel.leadership_position, members_leadership_rel.elected_date;";
        $result = $db->query($query);
        $leadership = null;
        $processedLeadership = null;

        while($row = $result->fetch_assoc()){
            $leadership[] = (object)$row;
        }

        // Process the leaders to strip out timestamps, calculate term exiprations, check for expired leaders, etc
        try {
            foreach ($leadership as $leader) {

                // strip out time from datetime
                $leader->elected_date = (new DateTime($leader->elected_date))->format('Y-m-d');

                // calculate term expiration
                $leader->term_expiration = (new DateTime($leader->elected_date))->modify("+$leader->term_expiration")->format('Y-m-t');

                // Check to see if the leader is expired. If so, remove the leader from the return object, and update the database so they will not come through in future page loads
                if ((new DateTime($leader->term_expiration)) < (new DateTime())) {

                    $query = "UPDATE members_leadership_rel SET is_expired = 1 WHERE id = $leader->id;";
                    $db->query($query);
                } else {
                    $processedLeadership[] = $leader;
                }
            }
        } catch (Exception $e){
            $processedLeadership = [];
        }

        return $processedLeadership;
    }

    // Return an associative array of committees
    public function getCommittees(mysqli $db){
        $query = "SELECT members_committees_rel.id, members.member_first_name, members.member_last_name, members.member_number, committee_name, is_chair FROM members_committees_rel INNER JOIN members ON members_committees_rel.members_id = members.member_number INNER JOIN committees ON members_committees_rel.committee = committees.committee_name WHERE is_expired = 0 ORDER BY committee_name;";
        $result = $db->query($query);
        $committees = null;
        $sortedCommittees = null;

        while($row = $result->fetch_assoc()){
            $committees[] = (object)$row;
        }

        // set the value of lastcommittee equal to the first committee name
        $lastCommittee = $committees[0]->committee_name;

        // Create an associative array of committees and their members
        foreach($committees as $committee){
            if($committee->committee_name == $lastCommittee){
                $sortedCommittees[$lastCommittee][] = $committee;
            } else{
                $lastCommittee = $committee->committee_name;
                $sortedCommittees[$lastCommittee][] = $committee;
            }
        }

        return $sortedCommittees;
    }

    public function getCalendarEvents($calendarId, EntityManagerInterface $entity){

        $service = $this->connectToGoogleCalendarApi($entity);

        return $service->events->listEvents($calendarId,[
            'maxResults' => 100,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => (new DateTime())->format('c'),
            'timeMax' => (new DateTime())->modify('+ 30 days')->format('c'),
        ]);

    }

    public function addCalendarEvent(DateTime $startTime, DateTime $endTime, string $title, EntityManagerInterface $entity, $repeat = null){

        // Connect to the API
        $service = $this->connectToGoogleCalendarApi($entity);

        // Instantiate google's datetime objects
        $start = (new Google_Service_Calendar_EventDateTime());
        $end = (new Google_Service_Calendar_EventDateTime());
        $newEvent = new Google_Service_Calendar_Event();

        // Set start and end times for event
        $start->setDateTime(($startTime)->format('Y-m-d\TH:i:sP'));
        $end->setDateTime(($endTime)->format('Y-m-d\TH:i:sP'));

        // Assign them to the event along with the title and the repeat interval
        $newEvent->setStart($start);
        $newEvent->setEnd($end);
        $newEvent->setSummary($title);

        // Insert the event
        $service->events->insert($_ENV['GOOGLE_CALENDAR_ID'], $newEvent);

    }

    public function connectToGoogleCalendarApi(EntityManagerInterface $entity){
        $client = new Google_Client();
        $credentialJson = $entity->getRepository(GoogleServiceAccountJson::class)->findOneBy(['type' => 'service_account']);
        $credentialJson = $credentialJson->returnAsArray($credentialJson);
        try {
            $client->setAuthConfig($credentialJson);
        } catch (Google_Exception $e) {

        }
        $client->setScopes(Google_Service_Calendar::CALENDAR);

        return new Google_Service_Calendar($client);
    }

}
