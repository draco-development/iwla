<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MembersLeadershipRel
 *
 * @ORM\Table(name="members_leadership_rel", indexes={@ORM\Index(name="key4", columns={"leadership_position"}), @ORM\Index(name="key3", columns={"member_id"})})
 * @ORM\Entity
 */
class MembersLeadershipRel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="elected_date", type="datetime", nullable=false)
     */
    private $electedDate;

    /**
     * @var int
     *
     * @ORM\Column(name="is_expired", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $isExpired = '0';

    /**
     * @var Members
     *
     * @ORM\ManyToOne(targetEntity="Members")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="member_id", referencedColumnName="member_number")
     * })
     */
    private $member;

    /**
     * @var LeadershipPositions
     *
     * @ORM\ManyToOne(targetEntity="LeadershipPositions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="leadership_position", referencedColumnName="position")
     * })
     */
    private $leadershipPosition;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getElectedDate(): \DateTime
    {
        return $this->electedDate;
    }

    /**
     * @param \DateTime $electedDate
     */
    public function setElectedDate(\DateTime $electedDate): void
    {
        $this->electedDate = $electedDate;
    }

    /**
     * @return int
     */
    public function getIsExpired(): int
    {
        return $this->isExpired;
    }

    /**
     * @param int $isExpired
     */
    public function setIsExpired(int $isExpired): void
    {
        $this->isExpired = $isExpired;
    }

    /**
     * @return Members
     */
    public function getMember(): Members
    {
        return $this->member;
    }

    /**
     * @param Members $member
     */
    public function setMember(Members $member): void
    {
        $this->member = $member;
    }

    /**
     * @return LeadershipPositions
     */
    public function getLeadershipPosition(): LeadershipPositions
    {
        return $this->leadershipPosition;
    }

    /**
     * @param LeadershipPositions $leadershipPosition
     */
    public function setLeadershipPosition(LeadershipPositions $leadershipPosition): void
    {
        $this->leadershipPosition = $leadershipPosition;
    }


}
