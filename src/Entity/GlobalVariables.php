<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalVariables
 *
 * @ORM\Table(name="global_variables")
 * @ORM\Entity
 */
class GlobalVariables
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="global_name", type="string", length=255, nullable=false)
     */
    private $globalName;

    /**
     * @var string
     *
     * @ORM\Column(name="global_value", type="string", length=255, nullable=false)
     */
    private $globalValue;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGlobalName(): string
    {
        return $this->globalName;
    }

    /**
     * @param string $globalName
     */
    public function setGlobalName(string $globalName): void
    {
        $this->globalName = $globalName;
    }

    /**
     * @return string
     */
    public function getGlobalValue(): string
    {
        return $this->globalValue;
    }

    /**
     * @param string $globalValue
     */
    public function setGlobalValue(string $globalValue): void
    {
        $this->globalValue = $globalValue;
    }

}
