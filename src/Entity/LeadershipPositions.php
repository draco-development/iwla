<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeadershipPositions
 *
 * @ORM\Table(name="leadership_positions", indexes={@ORM\Index(name="position", columns={"position"})})
 * @ORM\Entity
 */
class LeadershipPositions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=20, nullable=false)
     */
    private $position;

    /**
     * @var string|null
     *
     * @ORM\Column(name="term_expiration", type="string", length=20, nullable=true)
     */
    private $termExpiration;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition(string $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string|null
     */
    public function getTermExpiration(): ?string
    {
        return $this->termExpiration;
    }

    /**
     * @param string|null $termExpiration
     */
    public function setTermExpiration(?string $termExpiration): void
    {
        $this->termExpiration = $termExpiration;
    }


}
