<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Members
 *
 * @ORM\Table(name="members", indexes={@ORM\Index(name="member_number", columns={"member_number"})})
 * @ORM\Entity
 */
class Members
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="member_number", type="integer", nullable=false)
     */
    private $memberNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="member_first_name", type="string", length=20, nullable=false)
     */
    private $memberFirstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="member_middle_initial", type="string", length=10, nullable=true)
     */
    private $memberMiddleInitial;

    /**
     * @var string
     *
     * @ORM\Column(name="member_last_name", type="string", length=20, nullable=false)
     */
    private $memberLastName;

    /**
     * @var string
     *
     * @ORM\Column(name="membership_type", type="string", length=10, nullable=false)
     */
    private $membershipType;

    /**
     * @var string
     *
     * @ORM\Column(name="membership_status", type="string", length=10, nullable=false, options={"default"="Active"})
     */
    private $membershipStatus = 'Active';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date", type="datetime", nullable=false)
     */
    private $expirationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="join_date", type="datetime", nullable=false)
     */
    private $joinDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="member_birthdate", type="datetime", nullable=true)
     */
    private $memberBirthdate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="family_member_number", type="integer", nullable=true)
     */
    private $familyMemberNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="primary_member", type="integer", nullable=false, options={"default"="1"})
     */
    private $primaryMember = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="address1", type="string", length=50, nullable=true)
     */
    private $address1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address2", type="string", length=50, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=20, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=2, nullable=false)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=10, nullable=false)
     */
    private $zip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="occupation", type="string", length=255, nullable=true)
     */
    private $occupation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="retired", type="integer", nullable=true)
     */
    private $retired;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nra_member_number", type="string", length=20, nullable=true)
     */
    private $nraMemberNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="able_to_volunteer_labor", type="integer", nullable=false)
     */
    private $ableToVolunteerLabor = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="able_to_volunteer_time", type="integer", nullable=false)
     */
    private $ableToVolunteerTime = '0';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMemberNumber(): int
    {
        return $this->memberNumber;
    }

    /**
     * @param int $memberNumber
     */
    public function setMemberNumber(int $memberNumber): void
    {
        $this->memberNumber = $memberNumber;
    }

    /**
     * @return string
     */
    public function getMemberFirstName(): string
    {
        return $this->memberFirstName;
    }

    /**
     * @param string $memberFirstName
     */
    public function setMemberFirstName(string $memberFirstName): void
    {
        $this->memberFirstName = $memberFirstName;
    }

    /**
     * @return string|null
     */
    public function getMemberMiddleInitial(): ?string
    {
        return $this->memberMiddleInitial;
    }

    /**
     * @param string|null $memberMiddleInitial
     */
    public function setMemberMiddleInitial(?string $memberMiddleInitial): void
    {
        $this->memberMiddleInitial = $memberMiddleInitial;
    }

    /**
     * @return string
     */
    public function getMemberLastName(): string
    {
        return $this->memberLastName;
    }

    /**
     * @param string $memberLastName
     */
    public function setMemberLastName(string $memberLastName): void
    {
        $this->memberLastName = $memberLastName;
    }

    /**
     * @return string
     */
    public function getMembershipType(): string
    {
        return $this->membershipType;
    }

    /**
     * @param string $membershipType
     */
    public function setMembershipType(string $membershipType): void
    {
        $this->membershipType = $membershipType;
    }

    /**
     * @return string
     */
    public function getMembershipStatus(): string
    {
        return $this->membershipStatus;
    }

    /**
     * @param string $membershipStatus
     */
    public function setMembershipStatus(string $membershipStatus): void
    {
        $this->membershipStatus = $membershipStatus;
    }

    /**
     * @return \DateTime
     */
    public function getExpirationDate(): \DateTime
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime $expirationDate
     */
    public function setExpirationDate(\DateTime $expirationDate): void
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return \DateTime
     */
    public function getJoinDate(): \DateTime
    {
        return $this->joinDate;
    }

    /**
     * @param \DateTime $joinDate
     */
    public function setJoinDate(\DateTime $joinDate): void
    {
        $this->joinDate = $joinDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getMemberBirthdate(): ?\DateTime
    {
        return $this->memberBirthdate;
    }

    /**
     * @param \DateTime|null $memberBirthdate
     */
    public function setMemberBirthdate(?\DateTime $memberBirthdate): void
    {
        $this->memberBirthdate = $memberBirthdate;
    }

    /**
     * @return int|null
     */
    public function getFamilyMemberNumber(): ?int
    {
        return $this->familyMemberNumber;
    }

    /**
     * @param int|null $familyMemberNumber
     */
    public function setFamilyMemberNumber(?int $familyMemberNumber): void
    {
        $this->familyMemberNumber = $familyMemberNumber;
    }

    /**
     * @return int
     */
    public function getPrimaryMember(): int
    {
        return $this->primaryMember;
    }

    /**
     * @param int $primaryMember
     */
    public function setPrimaryMember(int $primaryMember): void
    {
        $this->primaryMember = $primaryMember;
    }

    /**
     * @return string|null
     */
    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    /**
     * @param string|null $address1
     */
    public function setAddress1(?string $address1): void
    {
        $this->address1 = $address1;
    }

    /**
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     */
    public function setAddress2(?string $address2): void
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip(string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string|null
     */
    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    /**
     * @param string|null $occupation
     */
    public function setOccupation(?string $occupation): void
    {
        $this->occupation = $occupation;
    }

    /**
     * @return int|null
     */
    public function getRetired(): ?int
    {
        return $this->retired;
    }

    /**
     * @param int|null $retired
     */
    public function setRetired(?int $retired): void
    {
        $this->retired = $retired;
    }

    /**
     * @return string|null
     */
    public function getNraMemberNumber(): ?string
    {
        return $this->nraMemberNumber;
    }

    /**
     * @param string|null $nraMemberNumber
     */
    public function setNraMemberNumber(?string $nraMemberNumber): void
    {
        $this->nraMemberNumber = $nraMemberNumber;
    }

    /**
     * @return int
     */
    public function getAbleToVolunteerLabor(): int
    {
        return $this->ableToVolunteerLabor;
    }

    /**
     * @param int $ableToVolunteerLabor
     */
    public function setAbleToVolunteerLabor(int $ableToVolunteerLabor): void
    {
        $this->ableToVolunteerLabor = $ableToVolunteerLabor;
    }

    /**
     * @return int
     */
    public function getAbleToVolunteerTime(): int
    {
        return $this->ableToVolunteerTime;
    }

    /**
     * @param int $ableToVolunteerTime
     */
    public function setAbleToVolunteerTime(int $ableToVolunteerTime): void
    {
        $this->ableToVolunteerTime = $ableToVolunteerTime;
    }



}
