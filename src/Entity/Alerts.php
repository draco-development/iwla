<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alerts
 *
 * @ORM\Table(name="alerts")
 * @ORM\Entity
 */
class Alerts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alert", type="text", length=65535, nullable=false)
     */
    private $alert;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expiration", type="date", nullable=true)
     */
    private $expiration;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAlert(): string
    {
        return $this->alert;
    }

    /**
     * @param string $alert
     */
    public function setAlert(string $alert): void
    {
        $this->alert = $alert;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiration(): ?\DateTime
    {
        return $this->expiration;
    }

    /**
     * @param \DateTime|null $expiration
     */
    public function setExpiration(?\DateTime $expiration): void
    {
        $this->expiration = $expiration;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }


}
