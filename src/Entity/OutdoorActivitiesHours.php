<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OutdoorActivitiesHours
 *
 * @ORM\Table(name="outdoor_activities_hours")
 * @ORM\Entity
 */
class OutdoorActivitiesHours
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="string", length=30, nullable=false)
     */
    private $activity;

    /**
     * @var bool
     *
     * @ORM\Column(name="day", type="string", nullable=false, options={"comment"="0=sunday"})
     */
    private $day;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="opentime", type="datetime", nullable=true)
     */
    private $opentime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="closingtime", type="datetime", nullable=true)
     */
    private $closingtime;

    /**
     * @var bool
     *
     * @ORM\Column(name="closed", type="boolean", nullable=false)
     */
    private $closed;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getActivity(): string
    {
        return $this->activity;
    }

    /**
     * @param string $activity
     */
    public function setActivity(string $activity): void
    {
        $this->activity = $activity;
    }

    /**
     * @return bool
     */
    public function isDay()
    {
        return (string) $this->day;
    }

    /**
     * @param bool $day
     */
    public function setDay(string $day): void
    {
        $this->day = $day;
    }

    /**
     * @return \DateTime|null
     */
    public function getOpentime(): ?\DateTime
    {
        return $this->opentime;
    }

    /**
     * @param \DateTime|null $opentime
     */
    public function setOpentime(?\DateTime $opentime): void
    {
        $this->opentime = $opentime;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosingtime(): ?\DateTime
    {
        return $this->closingtime;
    }

    /**
     * @param \DateTime|null $closingtime
     */
    public function setClosingtime(?\DateTime $closingtime): void
    {
        $this->closingtime = $closingtime;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    /**
     * @param bool $closed
     */
    public function setClosed(bool $closed): void
    {
        $this->closed = $closed;
    }


}
