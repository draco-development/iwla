<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RangeAccess
 *
 * @ORM\Table(name="range_access")
 * @ORM\Entity
 */
class RangeAccess
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="member_id", type="integer", nullable=false)
     */
    private $memberId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="safety_class_expiration", type="datetime", nullable=false)
     */
    private $safetyClassExpiration;

    /**
     * @var string|null
     *
     * @ORM\Column(name="safety_class_score", type="string", length=3, nullable=true)
     */
    private $safetyClassScore;

    /**
     * @var int
     *
     * @ORM\Column(name="is_suspended", type="integer", nullable=false)
     */
    private $isSuspended = '0';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMemberId(): int
    {
        return $this->memberId;
    }

    /**
     * @param int $memberId
     */
    public function setMemberId(int $memberId): void
    {
        $this->memberId = $memberId;
    }

    /**
     * @return \DateTime
     */
    public function getSafetyClassExpiration(): \DateTime
    {
        return $this->safetyClassExpiration;
    }

    /**
     * @param \DateTime $safetyClassExpiration
     */
    public function setSafetyClassExpiration(\DateTime $safetyClassExpiration): void
    {
        $this->safetyClassExpiration = $safetyClassExpiration;
    }

    /**
     * @return string|null
     */
    public function getSafetyClassScore(): ?string
    {
        return $this->safetyClassScore;
    }

    /**
     * @param string|null $safetyClassScore
     */
    public function setSafetyClassScore(?string $safetyClassScore): void
    {
        $this->safetyClassScore = $safetyClassScore;
    }

    /**
     * @return int
     */
    public function getIsSuspended(): int
    {
        return $this->isSuspended;
    }

    /**
     * @param int $isSuspended
     */
    public function setIsSuspended(int $isSuspended): void
    {
        $this->isSuspended = $isSuspended;
    }


}
