<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MembershipTypes
 *
 * @ORM\Table(name="membership_types")
 * @ORM\Entity
 */
class MembershipTypes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="membership_type", type="string", length=255, nullable=false)
     */
    private $membershipType;

    /**
     * @var string
     *
     * @ORM\Column(name="membership_short_name", type="string", length=15, nullable=false)
     */
    private $membershipShortName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="membership_notes", type="string", length=255, nullable=true)
     */
    private $membershipNotes;

    /**
     * @var int
     *
     * @ORM\Column(name="membership_fee", type="integer", nullable=false)
     */
    private $membershipFee;

    /**
     * @var int
     *
     * @ORM\Column(name="membership_fee_discounted", type="integer", nullable=false)
     */
    private $membershipFeeDiscounted;

    /**
     * @var string
     *
     * @ORM\Column(name="membership_code", type="string", length=3, nullable=false)
     */
    private $membershipCode;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMembershipType(): string
    {
        return $this->membershipType;
    }

    /**
     * @param string $membershipType
     */
    public function setMembershipType(string $membershipType): void
    {
        $this->membershipType = $membershipType;
    }

    /**
     * @return string
     */
    public function getMembershipShortName(): string
    {
        return $this->membershipShortName;
    }

    /**
     * @param string $membershipShortName
     */
    public function setMembershipShortName(string $membershipShortName): void
    {
        $this->membershipShortName = $membershipShortName;
    }

    /**
     * @return string|null
     */
    public function getMembershipNotes(): ?string
    {
        return $this->membershipNotes;
    }

    /**
     * @param string|null $membershipNotes
     */
    public function setMembershipNotes(?string $membershipNotes): void
    {
        $this->membershipNotes = $membershipNotes;
    }

    /**
     * @return int
     */
    public function getMembershipFee(): int
    {
        return $this->membershipFee;
    }

    /**
     * @param int $membershipFee
     */
    public function setMembershipFee(int $membershipFee): void
    {
        $this->membershipFee = $membershipFee;
    }

    /**
     * @return int
     */
    public function getMembershipFeeDiscounted(): int
    {
        return $this->membershipFeeDiscounted;
    }

    /**
     * @param int $membershipFeeDiscounted
     */
    public function setMembershipFeeDiscounted(int $membershipFeeDiscounted): void
    {
        $this->membershipFeeDiscounted = $membershipFeeDiscounted;
    }

    /**
     * @return string
     */
    public function getMembershipCode(): string
    {
        return $this->membershipCode;
    }

    /**
     * @param string $membershipCode
     */
    public function setMembershipCode(string $membershipCode): void
    {
        $this->membershipCode = $membershipCode;
    }


}
