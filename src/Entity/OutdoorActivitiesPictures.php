<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OutdoorActivitiesPictures
 *
 * @ORM\Table(name="outdoor_activities_pictures", uniqueConstraints={@ORM\UniqueConstraint(name="unique_pics", columns={"path", "activity"})})
 * @ORM\Entity
 */
class OutdoorActivitiesPictures
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="string", length=30, nullable=false)
     */
    private $activity;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getActivity(): string
    {
        return $this->activity;
    }

    /**
     * @param string $activity
     */
    public function setActivity(string $activity): void
    {
        $this->activity = $activity;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }


}
