<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alerts
 *
 * @ORM\Table(name="links")
 * @ORM\Entity
 */
class Links
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link_address", type="text", length=255, nullable=false)
     */
    private $linkAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="link_display", type="text", length=255, nullable=false)
     */
    private $linkDisplay;

    /**
     * @var string
     *
     * @ORM\Column(name="priority", type="integer", length=2, nullable=false)
     */
    private $priority = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLinkAddress(): string
    {
        return $this->linkAddress;
    }

    /**
     * @param string $linkAddress
     */
    public function setLinkAddress(string $linkAddress): void
    {
        $this->linkAddress = $linkAddress;
    }

    /**
     * @return string
     */
    public function getLinkDisplay(): string
    {
        return $this->linkDisplay;
    }

    /**
     * @param string $linkDisplay
     */
    public function setLinkDisplay(string $linkDisplay): void
    {
        $this->linkDisplay = $linkDisplay;
    }

    /**
     * @return string
     */
    public function getPriority(): string
    {
        return $this->priority;
    }

    /**
     * @param string $priority
     */
    public function setPriority(string $priority): void
    {
        $this->priority = $priority;
    }

}
