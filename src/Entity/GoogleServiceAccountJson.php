<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alerts
 *
 * @ORM\Table(name="google_service_account_json")
 * @ORM\Entity
 */

class GoogleServiceAccountJson
{
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text", length=255, nullable=false)
     * @ORM\Id
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="project_id", type="text", length=255, nullable=false)
     * @ORM\Id
     */
    private $projectId;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="private_key_id", type="text", length=255, nullable=false)
     */
    private $privateKeyId;

    /**
     * @var string
     *
     * @ORM\Column(name="private_key", type="text", nullable=false)
     */
    private $privateKey;

    /**
     * @var string
     *
     * @ORM\Column(name="client_email", type="text", length=255, nullable=false)
     */
    private $clientEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="text", length=255, nullable=false)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="auth_uri", type="text", length=255, nullable=false)
     */
    private $authUri;

    /**
     * @var string
     *
     * @ORM\Column(name="token_uri", type="text", length=255, nullable=false)
     */
    private $tokenUri;

    /**
     * @var string
     *
     * @ORM\Column(name="auth_provider_x509_cert_url", type="text", length=255, nullable=false)
     */
    private $authProviderX509CertUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="client_x509_cert_url", type="text", length=255, nullable=false)
     */
    private $clientX509CertUrl;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getProjectId(): string
    {
        return $this->projectId;
    }

    /**
     * @param string $projectId
     */
    public function setProjectId(string $projectId): void
    {
        $this->projectId = $projectId;
    }

    /**
     * @return string
     */
    public function getPrivateKeyId(): string
    {
        return $this->privateKeyId;
    }

    /**
     * @param string $privateKeyId
     */
    public function setPrivateKeyId(string $privateKeyId): void
    {
        $this->privateKeyId = $privateKeyId;
    }

    /**
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->privateKey;
    }

    /**
     * @param string $privateKey
     */
    public function setPrivateKey(string $privateKey): void
    {
        $this->privateKey = $privateKey;
    }

    /**
     * @return string
     */
    public function getClientEmail(): string
    {
        return $this->clientEmail;
    }

    /**
     * @param string $clientEmail
     */
    public function setClientEmail(string $clientEmail): void
    {
        $this->clientEmail = $clientEmail;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getAuthUri(): string
    {
        return $this->authUri;
    }

    /**
     * @param string $authUri
     */
    public function setAuthUri(string $authUri): void
    {
        $this->authUri = $authUri;
    }

    /**
     * @return string
     */
    public function getTokenUri(): string
    {
        return $this->tokenUri;
    }

    /**
     * @param string $tokenUri
     */
    public function setTokenUri(string $tokenUri): void
    {
        $this->tokenUri = $tokenUri;
    }

    /**
     * @return string
     */
    public function getAuthProviderX509CertUrl(): string
    {
        return $this->authProviderX509CertUrl;
    }

    /**
     * @param string $authProviderX509CertUrl
     */
    public function setAuthProviderX509CertUrl(string $authProviderX509CertUrl): void
    {
        $this->authProviderX509CertUrl = $authProviderX509CertUrl;
    }

    /**
     * @return string
     */
    public function getClientX509CertUrl(): string
    {
        return $this->clientX509CertUrl;
    }

    /**
     * @param string $clientX509CertUrl
     */
    public function setClientX509CertUrl(string $clientX509CertUrl): void
    {
        $this->clientX509CertUrl = $clientX509CertUrl;
    }

    public function returnAsArray(GoogleServiceAccountJson $json){
        return [
            "type" => $json->getType(),
            "project_id" => $json->getProjectId(),
            "private_key_id" => $json->getPrivateKeyId(),
            "private_key" => $json->getPrivateKey(),
            "client_email" => $json->getClientEmail(),
            "client_id" => $json->getClientId(),
            "auth_uri" => $json->getAuthUri(),
            "token_uri" => $json->getTokenUri(),
            "auth_provider_x509_cert_url" => $json->getAuthProviderX509CertUrl(),
            "client_x509_cert_url" => $json->getClientX509CertUrl(),
        ];
    }
}
