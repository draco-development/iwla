<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alerts
 *
 * @ORM\Table(name="nearby_chapters")
 * @ORM\Entity
 */
class NearbyChapters
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="chapter_name", type="text", length=255, nullable=false)
     */
    private $chapterName;

    /**
     * @var string
     *
     * @ORM\Column(name="google_calendar_id", type="text", length=255, nullable=false)
     */
    private $google_calendar_id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getChapterName(): string
    {
        return $this->chapterName;
    }

    /**
     * @param string $chapterName
     */
    public function setChapterName(string $chapterName): void
    {
        $this->chapterName = $chapterName;
    }

    /**
     * @return string
     */
    public function getGoogleCalendarId(): string
    {
        return $this->google_calendar_id;
    }

    /**
     * @param string $google_calendar_id
     */
    public function setGoogleCalendarId(string $google_calendar_id): void
    {
        $this->google_calendar_id = $google_calendar_id;
    }

}
