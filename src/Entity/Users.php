<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class Users implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="member_number", type="integer", nullable=false)
     */
    private $memberNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=70, nullable=false, unique=true)
     */
    private $password;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    private $plainPassword;

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->memberNumber,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->memberNumber,
            $this->password,
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function getMemberNumber(){
        return $this->memberNumber;
    }

    public function setMemberNumber(int $memberNumber){
        $this->memberNumber = $memberNumber;
    }

    public function getPlainPassword(){
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword){
        $this->plainPassword = $plainPassword;
    }

    public function setPassword(string $password){
        $this->password = $password;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getRoles()
    {
        $db = mysqli_connect($_SERVER['DATABASE_HOST'], $_SERVER['DATABASE_USER'], $_SERVER['DATABASE_PASSWORD'], $_SERVER['DATABASE_NAME']);
        $query = "SELECT user_role FROM users WHERE member_number = $this->memberNumber;";
        $result = $db->query($query);
        return $result->fetch_assoc();
    }

    public function getSalt()
    {
        return null;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }


    public function getUsername()
    {
        return $this->memberNumber;
    }

    public function eraseCredentials()
    {
        $this->setPlainPassword('');
    }


}
