<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Committees
 *
 * @ORM\Table(name="committees", indexes={@ORM\Index(name="committee_name", columns={"committee_name"})})
 * @ORM\Entity
 */
class Committees
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="committee_name", type="string", length=30, nullable=false)
     */
    private $committeeName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCommitteeName(): string
    {
        return $this->committeeName;
    }

    /**
     * @param string $committeeName
     */
    public function setCommitteeName(string $committeeName): void
    {
        $this->committeeName = $committeeName;
    }


}
