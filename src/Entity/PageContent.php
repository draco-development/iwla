<?php


namespace App\Entity;


class PageContent
{
    private $activityDetails;
    private $activityEvents;
    private $activityHours;
    private $activityLeadership;
    private $activityPictures;

    /**
     * @return mixed
     */
    public function getActivityDetails()
    {
        return $this->activityDetails;
    }

    /**
     * @param mixed $activityDetails
     */
    public function setActivityDetails($activityDetails): void
    {
        $this->activityDetails = $activityDetails;
    }

    /**
     * @return mixed
     */
    public function getActivityEvents()
    {
        return $this->activityEvents;
    }

    /**
     * @param mixed $activityEvents
     */
    public function setActivityEvents($activityEvents): void
    {
        $this->activityEvents = $activityEvents;
    }

    /**
     * @return mixed
     */
    public function getActivityHours()
    {
        return $this->activityHours;
    }

    /**
     * @param mixed $activityHours
     */
    public function setActivityHours($activityHours): void
    {
        $this->activityHours = $activityHours;
    }

    /**
     * @return mixed
     */
    public function getActivityLeadership()
    {
        return $this->activityLeadership;
    }

    /**
     * @param mixed $activityLeadership
     * @param $users
     */
    public function setActivityLeadership($activityLeadership, $users): void
    {
        $leadership = array();

        foreach($activityLeadership as $leader) {

            foreach ($users as $user) {
                if ($user->getMemberNumber() == $leader->getMemberId()) {
                    $leadership[] = $user;
                }
            }
        }
        $this->activityLeadership = $leadership;
    }

    /**
     * @return mixed
     */
    public function getActivityPictures()
    {
        return $this->activityPictures;
    }

    /**
     * @param mixed $activityPictures
     */
    public function setActivityPictures($activityPictures): void
    {
        $this->activityPictures = $activityPictures;
    }


}
