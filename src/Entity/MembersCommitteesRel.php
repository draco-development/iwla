<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MembersCommitteesRel
 *
 * @ORM\Table(name="members_committees_rel", indexes={@ORM\Index(name="committee", columns={"committee"}), @ORM\Index(name="key", columns={"members_id"})})
 * @ORM\Entity
 */
class MembersCommitteesRel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="is_chair", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $isChair;

    /**
     * @var int
     *
     * @ORM\Column(name="is_expired", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $isExpired;

    /**
     * @var Members
     *
     * @ORM\ManyToOne(targetEntity="Members")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="members_id", referencedColumnName="member_number")
     * })
     */
    private $members;

    /**
     * @var Committees
     *
     * @ORM\ManyToOne(targetEntity="Committees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="committee", referencedColumnName="committee_name")
     * })
     */
    private $committee;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIsChair(): int
    {
        return $this->isChair;
    }

    /**
     * @param int $isChair
     */
    public function setIsChair(int $isChair): void
    {
        $this->isChair = $isChair;
    }

    /**
     * @return int
     */
    public function getIsExpired(): int
    {
        return $this->isExpired;
    }

    /**
     * @param int $isExpired
     */
    public function setIsExpired(int $isExpired): void
    {
        $this->isExpired = $isExpired;
    }

    /**
     * @return Members
     */
    public function getMembers(): Members
    {
        return $this->members;
    }

    /**
     * @param Members $members
     */
    public function setMembers(Members $members): void
    {
        $this->members = $members;
    }

    /**
     * @return Committees
     */
    public function getCommittee(): Committees
    {
        return $this->committee;
    }

    /**
     * @param Committees $committee
     */
    public function setCommittee(Committees $committee): void
    {
        $this->committee = $committee;
    }


}
