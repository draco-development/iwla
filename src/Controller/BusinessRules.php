<?php
/**
 * Created by IntelliJ IDEA.
 * User: Edholm
 * Date: 9/4/18
 * Time: 10:32 PM
 */

namespace App\Controller;


use App\Entity\Alerts;
use App\Entity\Globals;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BusinessRules extends AbstractController
{

    protected $alerts;
    protected $session;
    protected $logger;
    protected $entity;
    protected $iwla_db;
    protected $cache;
    protected $globals;

    protected $membershipDiscountBeginDate; // GLOBAL - DATABASE DRIVEN
    protected $membershipDiscountEndDate; // GLOBAL - DATABASE DRIVEN
    protected $contactFormRecipient; // GLOBAL - DATABASE DRIVEN
    protected $chargeCCFees; // GLOBAL - DATABASE DRIVEN

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entity, SessionInterface $session)
    {
        date_default_timezone_set ($_ENV['GOOGLE_CALENDAR_TIMEZONE']);
        $session->start();
        $cache = new FilesystemCache();
        $this->session = $session;
        $this->logger = $logger;
        $this->entity = $entity;
        $this->alerts = $this->setAlerts();
        $this->globals = (new Globals())->cleanGlobals($this->entity->getRepository(Globals::class)->findAll());
        $this->iwla_db = mysqli_connect($_SERVER['DATABASE_HOST'], $_SERVER['DATABASE_USER'], $_SERVER['DATABASE_PASSWORD'], $_SERVER['DATABASE_NAME']);
        $this->iwla_db->set_charset('utf8');
        $this->cache = $cache;

        if($_SERVER['APP_ENV'] == 'dev'){$cache->clear();}
        //Set globals and cache so we can skip this database call later
        if(!$cache->has('globalsSet')){
            $this->setGlobals(3600);
        } else {

            // if the cache is set, use it
            $this->membershipDiscountBeginDate = $cache->get('membershipDiscountBeginDate');
            $this->membershipDiscountEndDate = $cache->get('membershipDiscountEndDate');
            $this->contactFormRecipient = $cache->get('contactFormRecipient');
            $this->chargeCCFees = $cache->get('chargeCCFees');
        }

    }

    // Check for any alerts in the database. Alerts are for admins to set for urgent messages
    public function setAlerts(){ // Null values for expiration are alerts that will never expire

        $returnAlerts = false;

        // Select all active alerts
        $alerts = $this->entity->getRepository(Alerts::class)->findBy(["isActive" => 1]);

         //Loop through each alert to return it to the page
         foreach ($alerts as $alert) {

             $expiration = $alert->getExpiration();
             // check to see if the alert has been left as "active" but is expired
             if ($expiration < (new DateTime()) && $expiration !== null && $alert->isActive()) {
                 // Mark the expired alert as inactive for future page loads
                 $alert->setIsActive(0);
                 $this->entity->persist($alert);

             } else {
                 // add the alert into the return array
                 $returnAlerts[] = $alert->getAlert();
             }
         }

        // Submit any updates necessary
        $this->entity->flush();

        // Return the array of alerts
        return $returnAlerts;
    }

    // This function gets any alers that need displayed. Alerts are only displayed once per session.
    public function getAlerts(){
        if($this->session->get('alertsDisplayed')){
            return false;
        } else {
            $this->session->set('alertsDisplayed', true);
            return $this->alerts;
        }
    }

    // This method sets the global variables from the database into the cache with a custom TTL
    private function setGlobals($ttl = 3600){
        // Grab all Globals from database
        $query = "SELECT global_name, global_value FROM global_variables;";
        $result = $this->iwla_db->query($query);

        // Assign them to the proper variable
        while($row = $result->fetch_assoc()){
            switch ($row['global_name']){
                case 'membership_discount_begin_date':
                    $this->membershipDiscountBeginDate = (new DateTime())->format('Y'.$row['global_value']);
                    $this->cache->set('membershipDiscountBeginDate', $this->membershipDiscountBeginDate);
                    break;
                case 'membership_discount_end_date':
                    $this->membershipDiscountEndDate = (new DateTime())->format('Y'.$row['global_value']);
                    $this->cache->set('membershipDiscountEndDate', $this->membershipDiscountEndDate);
                    break;
                case 'contact_form_recipient':
                    $this->contactFormRecipient = $row['global_value'];
                    $this->cache->set('contactFormRecipient', $this->contactFormRecipient);
                    break;
                case 'charge_cc_fees':
                    $this->chargeCCFees = $row['global_value'];
                    $this->cache->set('chargeCCFees', $this->chargeCCFees);
                    break;
                default:
                    break;
            }
        }
        $this->cache->set('globalsSet', true, $ttl);
    }

    // Custom method to call APIs via POST quickly, can choose to json decode response (on by default)
    public function postToURL($path, $payload, $authorization = null , $jsonDecode = true){

        $ch = curl_init( $path );

        // Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json',
            "$authorization"
        ]);

        // Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        // Send request.
        $result = curl_exec($ch);
        curl_close($ch);

        if($jsonDecode){
            return json_decode($result);
        }

        return $result;

    }

    // This method processes the form located on /join
    public function processNewMemberForm(Request $request){

        $type = $request->get('value');
        $currentDate = (new DateTime())->format('Ymd');
        $membershipType = ucwords($request->get('value'));

        $query = "SELECT membership_fee, membership_fee_discounted FROM membership_types WHERE membership_code = '$type' OR membership_type = 'Range Access';";
        $result = $this->iwla_db->query($query);

        // The first result in the parallel arrays will be the membership fees for the selected membership type. The second result will be the range fee
        while($row = $result->fetch_array()){
            $membershipFees[] = $row[0];
            $membershipFeeDiscs[] = $row[1];
        }


        if($currentDate >= $this->membershipDiscountBeginDate && $currentDate <= $this->membershipDiscountEndDate){
            $membershipDues = $membershipFeeDiscs[0];
            $rangeFee = $membershipFeeDiscs[1];
        } else {
            $membershipDues = $membershipFees[0];
            $rangeFee = $membershipFees[1];
        }

        $line_items[] = [
            'name' => "$membershipType Membership",
            'quantity' => "1",
            'base_price_money' => [
                'amount' => (int)$membershipDues*100, // have to multiply by 100 since form is in dollars and api is in cents
                'currency' => 'USD'
            ],
        ];

        if($request->get('range') == '1'){
            $line_items[] = [
                'name' => "Range Access",
                'quantity' => "1",
                'base_price_money' => [
                    'amount' => (int)$rangeFee*100, // have to multiply by 100 since form is in dollars and api is in cents
                    'currency' => 'USD'
                ],
            ];
        }

        if($this->chargeCCFees){

            if($request->get('range') != 'yes'){
                $rangeFee = 0;
            }
            $ccfee = (($membershipDues*100)+($rangeFee*100))*.035;
            $line_items[] = [
                'name' => "Credit Card Processing Fee",
                'quantity' => "1",
                'base_price_money' => [
                    'amount' => (int)$ccfee, // have to multiply by 100 since form is in dollars and api is in cents
                    'currency' => 'USD'
                ],
            ];
        }


        return $line_items;
    }

    // This method handles the user once they return back from square while attempting to join
    public function addNewMember(Request $request){
        $firstName = $this->iwla_db->real_escape_string($request->get('firstName'));
        $middleInit = $this->iwla_db->real_escape_string($request->get('middleInit'));
        $lastName = $this->iwla_db->real_escape_string($request->get('lastName'));
        $dob = $this->iwla_db->real_escape_string($request->get('dob'));
        $membershipType = $this->iwla_db->real_escape_string($request->get('value'));
        $rangeAccess = $this->iwla_db->real_escape_string($request->get('range'));
        $email = $this->iwla_db->real_escape_string($request->get('email'));
        $cellphone = $this->iwla_db->real_escape_string($request->get('cellphone'));
        $homephone = $this->iwla_db->real_escape_string($request->get('homephone'));
        $nra = $this->iwla_db->real_escape_string($request->get('nra'));
        $computerSkills = $request->get('computer'); //TODO No current solution in the database
        $occupation = $this->iwla_db->real_escape_string($request->get('occupation'));
        if($this->iwla_db->real_escape_string($request->get('retired'))){
            $retired = 1;
        } else {
            $retired = 0;
        }
        $time = $this->iwla_db->real_escape_string($request->get('time'));
        $labor = $this->iwla_db->real_escape_string($request->get('labor'));

        $spouseFirstName = $this->iwla_db->real_escape_string($request->get('spouseFirstName'));
        $spouseMiddleInit = $this->iwla_db->real_escape_string($request->get('spouseMiddleInit'));
        $spouseLastName = $this->iwla_db->real_escape_string($request->get('spouseLastName'));
        $spouseDob = $this->iwla_db->real_escape_string($request->get('spousedob'));
        $spouseEmail = $this->iwla_db->real_escape_string($request->get('spouseEmail'));
        $spouseCellphone = $this->iwla_db->real_escape_string($request->get('spouseCellphone'));
        $spouseHomephone = $this->iwla_db->real_escape_string($request->get('spouseHomephone'));
        $spouseNra = $this->iwla_db->real_escape_string($request->get('spouseNra'));
        $spouseComputerSkills = $request->get('spouseComputer'); //TODO No current solution in the database
        $spouseOccupation = $this->iwla_db->real_escape_string($request->get('spouseOccupation'));
        if($this->iwla_db->real_escape_string($request->get('spouseRetired'))){
            $spouseRetired = 1;
        } else {
            $spouseRetired = 0;
        }
        $spouseTime = $this->iwla_db->real_escape_string($request->get('spouseTime'));
        $spouseLabor = $this->iwla_db->real_escape_string($request->get('spouseLabor'));


        $children = $request->get('childName'); //TODO no current solution in database

        foreach($children as $child){
            $cleanChildren[] = $this->iwla_db->real_escape_string($child);
        }

        $address1 = $this->iwla_db->real_escape_string($request->get('address1'));
        $address2 = $this->iwla_db->real_escape_string($request->get('address2'));
        $city = $this->iwla_db->real_escape_string($request->get('city'));
        $state = $this->iwla_db->real_escape_string($request->get('state'));
        $zip = $this->iwla_db->real_escape_string($request->get('zip'));

        foreach($computerSkills as $computerSkill){
            $cleanComputerSkills[] = $this->iwla_db->real_escape_string($computerSkill);
        }

        foreach($spouseComputerSkills as $computerSkill){
            $spouseCleanComputerSkills[] = $this->iwla_db->real_escape_string($computerSkill);
        }

        $expirationDate = null;
        $today = null;

        try {
            $today = (new DateTime())->format('Ymd');
            if($today <= $this->membershipDiscountEndDate){
                // Date is during the first 2 sections of the year
                $expirationDate = (new DateTime('Y1231'))->format('Y-12-31');
            } else {
                // Date is during the full year's membership gets the rest of this year and next
                $expirationDate = (new DateTime('Y1231'))->modify('+1 year')->format('Y-12-31');
            }
        } catch (\Exception $e) {
            return false;
        }

        $memberNumber = $this->getTemporaryMemberNumber();

        if($membershipType == 'FM'){
            $familyMemberNumber = $this->getTemporaryMemberNumber($memberNumber);
        } else {
            $familyMemberNumber = null;
        }

        // Insert the new member
        $query = "INSERT INTO members (member_number, member_first_name, member_middle_initial,member_last_name,membership_type,membership_status,expiration_date, join_date, member_birthdate, family_member_number,primary_member,address1, address2, city, state, zip, email, phone, cellphone, occupation, retired, nra_member_number, able_to_volunteer_labor, able_to_volunteer_time,range_access) VALUES ($memberNumber, '$firstName','$middleInit','$lastName','$membershipType','Temporary','$expirationDate','$today','$dob','$familyMemberNumber', 1,'$address1','$address2','$city','$state','$zip','$email','$homephone','$cellphone','$occupation',$retired,'$nra', $labor, $time,'$rangeAccess');";
        $this->iwla_db->query($query);

        // Insert the family member if a family membership was chosen and a spouse was submitted
        if($membershipType == 'FM' && $spouseFirstName != null) {
            $query = "INSERT INTO members (member_number, member_first_name, member_middle_initial,member_last_name,membership_type,membership_status,expiration_date, join_date, member_birthdate, family_member_number, primary_member,address1, address2, city, state, zip, email, phone, cellphone, occupation, retired, nra_member_number, able_to_volunteer_labor, able_to_volunteer_time, range_access) VALUES ($familyMemberNumber, '$spouseFirstName','$spouseMiddleInit','$spouseLastName','$membershipType','Temporary','$expirationDate','$today','$spouseDob','$memberNumber', 0,'$address1','$address2','$city','$state','$zip','$spouseEmail','$spouseHomephone','$spouseCellphone','$spouseOccupation', $spouseRetired,'$spouseNra', $spouseLabor, $spouseTime, $rangeAccess);";
            $this->iwla_db->query($query);
        }

        return true;
    }

    // This method handles the user once they return back from square while attempting to donate
    public function processDonation(Request $request){
        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $donation = $request->get('value');

        $transport = (new \Swift_SmtpTransport($_SERVER['MAILER_HOST'], $_SERVER['MAILER_PORT'], $_SERVER['MAILER_ENCRYPTION']))
            ->setUsername($_SERVER['MAILER_USERNAME'])
            ->setPassword($_SERVER['MAILER_PASSWORD']);

        $mailer = new \Swift_Mailer($transport);

        try {
            $message = (new \Swift_Message('Contact Form from iwla-fw.com'))
                ->setFrom('no-reply@edholm.me')
                ->setTo($this->contactFormRecipient)
                ->setReplyTo($request->request->get('email'))
                ->setBody(
                    $this->renderView('email/donation.html.twig', [
                            'name' => $name,
                            'email' => $email,
                            'phone' => $phone,
                            'donation' => $donation,
                        ]
                    ),
                    'text/html'
                );
        } catch(\Exception $e){
            $e->getMessage();
        }
        if($mailer->send($message)){
            $this->addFlash('notice', 'Thank you so much for your donation! We are emailing you a receipt for your records.');
            return true;
        } else {
            $this->logger->info($request->request->get('email').' could not send donation receipt at '. (new DateTime())->format('Y-m-d H:i:s'));
            return false;
        }

    }

    // This is a helper method that generates a random number guaranteed unique to the database. It takes one optional parameter that makes sure it does not equal that number as well.
    public function getTemporaryMemberNumber($notEqualTo = null){
        do {
            // Create a random member number and continue until we have a unique one
            $temporaryMemberNumber = intval("999" . rand(1,900000));
            $query = "SELECT id FROM members WHERE member_number = $temporaryMemberNumber;";
            $result = $this->iwla_db->query($query);
        } while($result->num_rows > 0 && $temporaryMemberNumber != $notEqualTo);

        return $temporaryMemberNumber;
    }

    // This method handles the form submission that is in the member center
    public function processRenewalForm(Request $request){

    }

    // This method handles the form submission located at /donate before handing them off to square
    public function processDonationForm(Request $request){

        $donation = $request->get('value');

        // strip out anything that isn't a number
        $value = $this->cleanDollar($donation);

        // Add it to the array
        $line_items[] = [
            'name' => 'Donation',
            'quantity' => "1",
            'base_price_money' => [
                'amount' => $value,
                'currency' => 'USD'
            ],
        ];

        return $line_items;
    }

    // This is a helper method to validate any dollar amount submitted to a form
    public function cleanDollar($value){

        // break the value up into an array of values
        $value = explode(".", $value);

        // remove the last part of the dollar amount
        if(sizeof($value) > 1) {
            array_pop($value);
        }
        // put the array back together without the last two after the decimal
        $value = implode("",$value);

        // Remove any extra characters that are not numbers
        $value = preg_replace("/[^0-9]/","", $value);

        // multiply by 100 since the form is requesting dollars, and the API returns Cents
        return (int)$value * 100;
    }


}
