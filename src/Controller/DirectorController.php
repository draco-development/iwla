<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 8/23/2018
 * Time: 15:29
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DirectorController
 * @package App\Controller
 * @Route("/director")
 */
class DirectorController extends BusinessRules
{
    /**
     * @Route("/", name="directorIndex")
     */
    public function boardIndex()
    {
        return $this->render('base.html.twig',[
            'alerts' => $this->alerts,
        ]);
    }
}