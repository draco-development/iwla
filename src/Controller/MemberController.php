<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 8/23/2018
 * Time: 15:26
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MemberController
 * @package App\Controller
 * @Route("/members")
 */
class MemberController extends BusinessRules
{
    /**
     * @Route("/", name="memberIndex")
     */
    public function memberHome()
    {
        return $this->render('base.html.twig',[
        'alerts' => $this->alerts,
        ]);
    }
}