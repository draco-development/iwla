<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 8/23/2018
 * Time: 15:29
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DirectorController
 * @package App\Controller
 * @Route("/rso")
 */
class RsoController extends BusinessRules
{
    /**
     * @Route("/", name="RsoIndex")
     */
    public function rsoIndex()
    {
        return $this->render('security/rso.html.twig',[
            'alerts' => $this->alerts,

        ]);
    }
}