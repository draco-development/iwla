<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 8/24/2018
 * Time: 16:23
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MemberController
 * @package App\Controller
 * @Route("/officer")
 */

class OfficerController extends BusinessRules
{
    /**
     * @Route("/", name="officerIndex")
     */
    public function boardIndex()
    {
        return $this->render('base.html.twig',[
            'alerts' => $this->alerts,

        ]);
    }
}