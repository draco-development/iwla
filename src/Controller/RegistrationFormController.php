<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 9/25/2018
 * Time: 10:20
 */

namespace App\Controller;

use App\Form\UserType;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class RegistrationFormController extends BusinessRules
{
    /**
     * @Route("/register", name="user_registration")
     * @param $request
     * @param $passwordEncoder
     * @param $entity
     * @return Response
     */
    public function register(Request $request, EntityManagerInterface $entity)
    {
        // 1) build the form
        $user = new Users();

        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password
            $password = password_hash($user->getPlainPassword(), PASSWORD_DEFAULT);
            $user->setPassword($password);
            $user->setIsActive(1);

            // 4) save the User!
            $entity->persist($user);

            // User roles need added. need users_id and user_roles_id (this from checkboxes in post)
            $entity->flush();

            // ... do any other work - like sending them an email, etc
            // We should probably log them in at this point
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('index', [
                'alerts' => $this->alerts,
            ]);
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
            'alerts' => $this->alerts,
        ]);
    }
}