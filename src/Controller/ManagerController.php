<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 8/24/2018
 * Time: 16:23
 */

namespace App\Controller;

use App\Entity\Activities;
use App\Entity\Members;
use App\Entity\OutdoorActivitiesEvents;
use App\Entity\OutdoorActivitiesHours;
use App\Entity\OutdoorActivitiesLeadership;
use App\Entity\OutdoorActivitiesPictures;
use App\Entity\PageContent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MemberController
 * @package App\Controller
 * @Route("/manager")
 */

class ManagerController extends BusinessRules
{
    /**
     * @Route("/", name="managerIndex")
     */
    public function boardIndex()
    {

        $activities = $this->entity->getRepository(Activities::class)->findBy(['isActive' => 1]);

        return $this->render('manager/managerIndex.html.twig',[
            'alerts' => $this->alerts,
            'activities' => $activities,
        ]);
    }


    /**
     * @Route("/activity/{slug}", name="manageActivity")
     */
    public function manageActivity($slug){

        $content = new PageContent();

        $activity = $this->entity->getRepository(Activities::class)->findOneBy(['slug' => $slug]);
        $events = $this->entity->getRepository(OutdoorActivitiesEvents::class)->findBy(['activity' => $activity->getName()]);
        $hours = $this->entity->getRepository(OutdoorActivitiesHours::class)->findBy(['activity' => $activity->getName()]);
        $leadership = $this->entity->getRepository(OutdoorActivitiesLeadership::class)->findBy(['activity' => $activity->getName()]);
        $users = $this->entity->getRepository(Members::class)->findAll();
        $pictures = $this->entity->getRepository(OutdoorActivitiesPictures::class)->findBy(['activity' => $activity->getName()]);

        $content->setActivityDetails($activity);
        $content->setActivityEvents($events);
        $content->setActivityHours($hours);
        $content->setActivityLeadership($leadership, $users);
        $content->setActivityPictures($pictures);

        return $this->render('manager/manageActivity.html.twig',[
            'alerts' => $this->alerts,
            'content' => $content,
        ]);


    }

}
