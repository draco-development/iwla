<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 10/3/2018
 * Time: 12:34
 */

namespace App\Controller;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class MemberController
 * @package App\Controller
 * @Route(methods={"POST"})
 */

class PostController extends BusinessRules
{
    /**
     * @param Request $request
     * @return bool
     */
    public function isEligibleToJoin(Request $request){

        /* There is no guaranteed way to catch somebody who is joining a second time.
           There are enough ways to variate the data so that an exact match could be near impossible to find.
           I will add a few checks to try and gather as many matches as I can without restricting people with names like John Smith from joining
        */

        $exists = false;

        $firstName = $request->request->get('firstName');
        $lastName = $request->request->get('lastName');
        $email = $request->request->get('email');
        $cell = $request->request->get('cellphone');
        $phone = $request->request->get('homephone');


        // Query for an exact match
        $query = "SELECT id, expiration_date FROM members WHERE member_first_name = ? AND member_last_name = ? AND email = ? AND ((phone = ? OR cellphone = ?) OR (phone = ? OR cellphone = ?));";
        $stmt = $this->iwla_db->prepare($query);
        $stmt->bind_param('sssssss', $firstName, $lastName, $email, $phone, $phone, $cell, $cell);
        $stmt->execute();
        $stmt->bind_result($exists, $expiration);
        $stmt->fetch();

        if(!$exists) {
            //Query for a last name and email match
            $query = "SELECT id, expiration_date FROM members WHERE member_last_name = ? AND email = ?;";
            $stmt = $this->iwla_db->prepare($query);
            $stmt->bind_param('ss', $lastName, $email);
            $stmt->execute();
            $stmt->bind_result($exists, $expiration);
            $stmt->fetch();
            if ($exists) {return false;}
        }

        if(!$exists) {
            //Query for just a matching email
            $query = "SELECT id, expiration_date FROM members WHERE email = ?;";
            $stmt = $this->iwla_db->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            $stmt->bind_result($exists, $expiration);
            $stmt->fetch();
        }

        if(!$exists) {
            //Query for a matching first, last and phone
            $query = "SELECT id, expiration_date FROM members WHERE member_first_name = ? AND member_last_name = ? AND ((phone = ? OR cellphone = ?) OR (phone = ? OR cellphone = ?));";
            $stmt = $this->iwla_db->prepare($query);
            $stmt->bind_param('ssssss', $firstName, $lastName, $phone, $phone, $cell, $cell);
            $stmt->execute();
            $stmt->bind_result($exists, $expiration);
            $stmt->fetch();
        }

        // If we found a user, lets check when they expired
        if($exists){
            try{
                $renewalDate = (new \DateTime($expiration))->modify('+2 years')->format('Ymd');
                $today = (new \DateTime())->format('Ymd');

                // We've found a previous user, lets check to see if more than 2 years have passes since their membership expired.
                if ($renewalDate > $today) {
                    return false; // not enough time has passed, they cannot join
                }

            } catch (\Exception $e){
                $this->logger->alert($e->getMessage());
            }
        }

        // This line will only fire if they are not a past member OR if they are a past member whose membership expired over 2 years ago or if the try/catch fails.
        return true;

    }

    /**
     * @Route("/form-contact", name="contactForm")
     * @param $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function contactForm(Request $request){

        $transport = (new \Swift_SmtpTransport($_SERVER['MAILER_HOST'], $_SERVER['MAILER_PORT'], $_SERVER['MAILER_ENCRYPTION']))
            ->setUsername($_SERVER['MAILER_USERNAME'])
            ->setPassword($_SERVER['MAILER_PASSWORD']);

            $mailer = new \Swift_Mailer($transport);

        try {
            $message = (new \Swift_Message('Contact Form from iwla-fw.com'))
                ->setFrom('no-reply@iwla-fw.com')
                ->setTo($this->contactFormRecipient)
                ->setReplyTo($request->request->get('email'))
                ->setBody(
                    $this->renderView('email/contact.html.twig', [
                            'name' => $request->request->get('name'),
                            'email' => $request->request->get('email'),
                            'comments' => $request->request->get('comments'),
                        ]
                    ),
                    'text/html'
                )
                ->addPart(
                    $this->renderView('email/contact.txt.twig', [
                            'name' => $request->request->get('name'),
                            'email' => $request->request->get('email'),
                            'comments' => $request->request->get('comments'),
                        ]
                    ),
                    'text/plain');
        } catch(\Exception $e){
            $e->getMessage();
        }
        if($mailer->send($message)){
            $this->addFlash('message', 'Message Sent!');
            return $this->redirectToRoute('contact');
        } else {
            $this->logger->info($request->request->get('email').' could not send email at '. (new \DateTime())->format('Y-m-d H:i:s'));
            $this->addFlash('error', 'Could not send your message at this time, please try again later');
            return $this->redirectToRoute('contact');
        }

    }

    /**
     * @Route("/payment", name="payment")
     * @param $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function payment(Request $request){

        $type = $request->get('type');
        $email = $request->get('email');
        $customid = sha1(rand(1,PHP_INT_MAX).$email.$type.random_bytes(50));

        $object = serialize($request);

        // Save request in the session so we can retrieve it once square is finished with it.
        $this->session->set($customid, $object);

        switch ($type){
            case 'new_member':
                if($this->isEligibleToJoin($request)){
                    $line_items = $this->processNewMemberForm($request);
                } else{
                    $this->addFlash(
                        'notice',
                        'You are already a member or your membership has simply expired. Please login and renew your membership or contact us for information'
                    );
                    return $this->redirectToRoute('login');
                }
                break;
            case 'donation':
                $line_items = $this->processDonationForm($request);
                break;
            case 'renew':
                $line_items = $this->processRenewalForm($request);
                break;
            default:
                return $this->redirectToRoute("index");
        }

        $this->session->set('guid',bin2hex(random_bytes(16)));
        $order = json_encode([
            'idempotency_key' => $this->session->get('guid'),
            'order' => [
                'reference_id' => $customid,
                'line_items' =>
                    $line_items,

            ],
            'merchant_support_email' => $this->session->get('COMPANY_ADMIN'),
            'pre_populate_buyer_email' => $email,
            'redirect_url' => $_SERVER['SQUARE_SUCCESSFUL_REDIRECT_PATH'],
        ]);

        $result = $this->postToURL($_SERVER['SQUARE_PATH'], $order,"Authorization:Bearer ".$_SERVER['SQUARE_PERSONAL_ACCESS_TOKEN']);

        // if there was an error processing the order, redirect the user to the index for right now.
        if(property_exists($result,"errors")) {

            // log all errors
            foreach($result->errors as $error){
                $this->logger->error($error->code);
                $this->logger->error($error->detail);
                $this->logger->info("Something did not come through properly when attempting to create an order.");
            }

            // Send user to an error page
            return $this->redirectToRoute("index");
        }

        // send the user to the payments page
        return $this->redirect($result->checkout->checkout_page_url);
    }

}