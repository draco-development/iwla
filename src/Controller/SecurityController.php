<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 8/24/2018
 * Time: 9:56
 */

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

/**
 * Class SecurityController
 * @package App\Controller
 */
class SecurityController extends BusinessRules  implements AccessDeniedHandlerInterface
{
    /**
     * @Route("/login", name="login")
     * @param $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'alerts'        => $this->alerts,
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(){

    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        // TODO: Add unauthorized error page.
        return $this->redirectToRoute('index');
    }

}