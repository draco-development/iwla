<?php
/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 8/9/2018
 * Time: 11:24
 */

namespace App\Controller;

use App\Entity\ChapterNeeds;
use App\Entity\Links;
use App\Entity\Activities;
use App\Entity\NearbyChapters;
use App\Entity\Newsletter;
use App\Service\HelperMethods;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @property-read \Mysqli iwla_db
 * @property-read AbstractController $logger
 */
class PublicController extends BusinessRules
{
    /**
     * @Route("/", name="index")
     * @param HelperMethods $helper
     * @return Response
     */
    public function index(HelperMethods $helper)
    {
        return $this->render("public/index.html.twig", [
            'alerts' => $this->getAlerts(),
            'rotatorImages' => $helper->getPageImages(5, "images/property/hiRes"),
        ]);
    }

    /**
     * @Route("/events/calendar", name="calendar")
     */
    public function calendar()
    {

        return $this->render('public/calendar.html.twig', [
            'alerts' => $this->getAlerts(),

        ]);
    }

    /**
     * @Route("/events/activities", name="activities")
     * @param HelperMethods $helpers
     * @return Response
     */
    public function activities(HelperMethods $helpers)
    {
        $activities = $helpers->getCalendarEvents($this->globals['google_calendar_id'], $this->entity);
        $showOtherChapterEvents = false;
        $otherChapters = [];

        if($this->globals['display_nearby_chapter_activities']){
            // This chapter wants to show activities happening at other chapters as well
            $showOtherChapterEvents = true;
            $nearbyChapters = $this->entity->getRepository(NearbyChapters::class)->findAll();
            foreach($nearbyChapters as $nearbyChapter) {
                $otherChapters[$nearbyChapter->getChapterName()] = $helpers->getCalendarEvents($nearbyChapter->getGoogleCalendarId(), $this->entity)->getItems();
            }
        }

        return $this->render('public/activities.html.twig', [
            'alerts' => $this->getAlerts(),
            'chapterEvents' => $activities->getItems(),
            'showLocalEvents' => $showOtherChapterEvents,
            'nearbyChapters' => $otherChapters,
        ]);
    }

    /**
     * @Route("/events/host-your-own-event", name="host-your-own-event")
     * @param HelperMethods $helper
     * @return Response
     */
    public function hostYourOwnEvent(HelperMethods $helper)
    {
        return $this->render('public/rental.html.twig', [
            'alerts' => $this->getAlerts(),
            'images' => $helper->getPageImages(5,"images/property/lowRes",["house", "property", "chapter"]),
            'rates' => 'rates',
        ]);
    }

    /**
     * @Route("/outdoor-activities/youth-trap-team", name="youthTeam")
     * @param HelperMethods $helper
     * @return Response
     */
    public function youthTeam(HelperMethods $helper){

        return $this->render('public/outdoorActivities.html.twig', [
            "images" => $helper->getPageImages(3, "images/property/lowRes", ["trap", "skeet", "shotgun", "horns",], false),
            'alerts' => $this->getAlerts(),
            'events' => $helper->getPageDetails("Trap And Skeet", $this->iwla_db),
            'hours' => $helper->getHoursOfOperation("Trap And Skeet", $this->iwla_db),
            'page' => 'Youth Trap Team',
            'synopsis' => 'This needs to be databased',
        ]);
    }

    /**
     * @Route("/outdoor-activities/{slug}", name="outdoorActivities")
     * @param $slug
     * @param HelperMethods $helper
     * @return Response
     */
    public function outdoorActivities($slug, HelperMethods $helper){

        $activityDesc = $this->entity->getRepository(Activities::class)->findOneBy(['slug' => $slug]);

        if(!$activityDesc){throw new NotFoundHttpException('That page was not found',null,404);}

        $keywords = explode(',', $activityDesc->getKeywords());
        $keywords[] = $slug;

        return $this->render('public/outdoorActivities.html.twig', [
            "images" => $helper->getPageImages(3, "images/property/lowRes", $keywords, false),
            'alerts' => $this->getAlerts(),
            'events' => $helper->getPageDetails($activityDesc->getName(), $this->iwla_db),
            'hours' => $helper->getHoursOfOperation($activityDesc->getName(), $this->iwla_db),
            'page' => $activityDesc->getName(),
            'synopsis' => $activityDesc->getDescription(),
        ]);
    }

    /**
     * @Route("/news/chapter-needs", name="needs")
     * @return Response
     */
    public function needs(){

        return $this->render("public/needs.html.twig", [
            'alerts' => $this->getAlerts(),
            'needs' => $this->entity->getRepository(ChapterNeeds::class)->findBy(['isActive' => 1]),
        ]);
    }

    /**
     * @Route("/news/newsletter", name="news")
     * @return Response
     */
    public function news(){

        return $this->render("public/newsletter.html.twig", [
            'alerts' => $this->getAlerts(),
            'newsletters' => $this->entity->getRepository(Newsletter::class)->findBy(['isActive' => 1]),
        ]);
    }

    /**
     * @Route("/news/links", name="links")
     * @return Response
     */
    public function links(){

        return $this->render("public/links.html.twig", [
            'alerts' => $this->getAlerts(),
            'links' => $this->entity->getRepository(Links::class)->findBy([], ['priority' => 'DESC', 'linkDisplay' => 'ASC']),

        ]);
    }

    /**
     * @Route("/about-us", name="about")
     * @return Response
     */
    public function about(){

        // Fetch all memberships from database
        $query = "SELECT membership_type, membership_notes, membership_fee FROM membership_types;";
        $result = $this->iwla_db->query($query);

        // define variables that could be unset if the query fails
        $memberships = array();
        $range = array();

        // loop through all memberships and assign them into the memberships array (also extract the range into it's own array)
        while($row = $result->fetch_array()){
            if($row[0] == 'Range Access'){$range = $row; continue;}
            $memberships[] = $row;
        }


        return $this->render("public/about.html.twig", [
            'alerts' => $this->getAlerts(),
            'memberships' => $memberships,
            'range' => $range,
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     * @param HelperMethods $helper
     * @return Response
     */
    public function contact(HelperMethods $helper){

        return $this->render("public/contact.html.twig", [
            'alerts' => $this->getAlerts(),
            'leaders' => $helper->getLeadership($this->iwla_db),
            'committees' => $helper->getCommittees($this->iwla_db),
        ]);
    }

    /**
     * @Route("/donate", name="donate")
     * @return Response
     */
    public function donate(){

        return $this->render("public/donate.html.twig", [
            'alerts' => $this->getAlerts(),

        ]);
    }

    /**
     * @Route("/join", name="join")
     * @return Response
     * @throws Exception
     */
    public function join(){

        $currentDate = (new DateTime())->format('Ymd');
        $query = "SELECT membership_type, membership_short_name, membership_notes, membership_fee, membership_fee_discounted, membership_code FROM membership_types;";
        $result = $this->iwla_db->query($query);

        $memberships = array();
        $range = array();

        // create an array with the proper membership rates
        if($currentDate >= $this->membershipDiscountBeginDate && $currentDate <= $this->membershipDiscountEndDate){
            // Date is during the 1/2 off section of the year
            while($row = $result->fetch_array()){
                $memberships[] = [$row[0],$row[1],$row[2],$row[4]." (Renews at \$$row[3])", $row[5]];
            }
        } else if($currentDate > $this->membershipDiscountEndDate) {
            // Date is during the full year's membership gets the rest of this year and next
            $expiration = (new DateTime())->modify('+2 years')->format('Y');
            while($row = $result->fetch_array()){
                $memberships[] = [$row[0],$row[1],$row[2],$row[3]." (Renews at \$$row[3] in January of $expiration!)", $row[5]];
            }
        } else {
            // Date is during the regular sign up period
            while($row = $result->fetch_array()){
                $memberships[] = [$row[0],$row[1],$row[2],$row[3]." (Renews at \$$row[3] in January)", $row[5]];
            }
        }

        // extract the range fee from the memberships to send to the page by itself
        $i = 0;
        foreach($memberships as $membership){
            if($membership[1] != 'range'){$i++;continue;}
            $range = $membership;
            unset($memberships[$i]);
            array_values($memberships);
            break;
        }


        return $this->render("public/join.html.twig", [
            'memberships' => $memberships,
            'alerts' => $this->getAlerts(),
            'range' => $range,
        ]);
    }

    /**
     * @Route("/sessionDestroy")
     * @param SessionInterface $session
     * @return RedirectResponse
     */
    public function destroy(SessionInterface $session){
        $session->clear();
        session_unset();
        session_destroy();

        return $this->redirectToRoute('index');
    }


    /**
     * @Route("/payments/thankyou", name="thankyou")
     * ?checkoutId={checkoutid}&referenceId={customid}&transactionId={transactionid}
     * @param Request $request
     * @return RedirectResponse
     */
    public function thankyou(Request $request){

        // Extract the query string params that Square sends us
        $checkoutid = $request->get('checkoutId');
        $referenceId = $request->get('referenceId'); // customId
        $transactionId = $request->get('transactionId');

        // Require those parameters above to view this page
        if($checkoutid == null || $referenceId == null || $transactionId == null){
            return $this->redirectToRoute('index');
        }

        //Retrieve the request from the session
        $request = unserialize($this->session->get($referenceId));

        $type = $request->get('type');

        //Remove the object from the session storage to save space
        $this->session->remove($referenceId);

        // Detect what form they posted with and perform the proper tasks
        switch ($type){
            case 'new_member':
                if(!$this->addNewMember($request)){
                    //if we cannot add the member, display an error and redirect them.
                    $this->addFlash('notice', 'Your payment was successful, but we could not process your membership at this time. Please contact us for assistance.');
                    return $this->redirectToRoute('contact');
                }
                break;
            case 'donation':
                if(!$this->processDonation($request)) {
                    //if we cannot create the receipt, display an error and redirect them.
                    $this->addFlash('notice', 'Your donation was successful, but we could not get you a receipt at this time. Please contact us for assistance.');
                    return $this->redirectToRoute('contact');
                }
                break;
            case 'renew':
                break;
            default:
                return $this->redirectToRoute("index");
        }

        // Return the user to the index page
        return $this->redirectToRoute('index');
    }
}
