Steps to properly clone this repository.<br>

<ol>
    <li>Clone using your preferred method (ssh or https)</li>
    <li>Using a Terminal window, cd in to the directory you just cloned</li>
    <li>Run the following commands: (you may have to omit .cmd from yarn commands)
    
    composer install
    composer dump-env dev
    yarn.cmd install
    yarn.cmd encore dev
     
   </li>
    <li>Open .env file and make sure that it matches the .env.dist that is in the repo    </li>
    <li>Run the server using <strong>one</strong> of the following:
        <ul>
            <li>Setup IntelliJ to run the server (adds debugging functionality)</li>
            <li>Run this command in Terminal window
            
    php -S 127.0.0.1:8888 -t public
   </li>
        </ul>
    </li>
</ol>
